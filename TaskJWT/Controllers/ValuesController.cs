﻿using System;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;
using TaskJWT.Models;

namespace TaskJWT.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TokenController : ControllerBase
    {
        private readonly IConfiguration _configuration;

        public TokenController(IConfiguration configuration)
        {
            _configuration = configuration;
        }

        [HttpGet]
        [Route("GenerateToken")]
        public IActionResult GenerateToken()
        {
            var key = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(
                  _configuration.GetSection("JwtConfig:Key").Value));

            var claims = new Claim[] {
                    new Claim(ClaimTypes.Role, "admin"),
                };

            var signingCredential = new SigningCredentials(
                key, SecurityAlgorithms.HmacSha256Signature);

            var tokenDescriptor = new SecurityTokenDescriptor
            {
                Subject = new ClaimsIdentity(claims),
                Expires = DateTime.UtcNow.AddMinutes(10),
                SigningCredentials = signingCredential
            };

            var tokenHandler = new JwtSecurityTokenHandler();

            var securityToken = tokenHandler.CreateToken(tokenDescriptor);

            string token = tokenHandler.WriteToken(securityToken);

            return Ok(token);

        }

        [HttpGet]
        [Authorize]
        [Route("ValidateToken")]
        public IActionResult ValidateToken()
        {
            return Ok("Congrats your Token Is Right");
        }
    }
}
